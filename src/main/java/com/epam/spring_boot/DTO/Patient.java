package com.epam.spring_boot.DTO;

import java.util.Objects;

public class Patient {

    private int idPatient;
    private String lastName;
    private String firstName;

    public Patient() {
    }

    public Patient(int idPatient, String lastName, String firstName) {
        this.idPatient = idPatient;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return idPatient == patient.idPatient &&
                Objects.equals(lastName, patient.lastName) &&
                Objects.equals(firstName, patient.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPatient, lastName, firstName);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "idPatient=" + idPatient +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
