package com.epam.spring_boot.repository.impl;

import com.epam.spring_boot.DTO.Patient;
import com.epam.spring_boot.repository.PatientDao;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PatientDaoImpl implements PatientDao {

    private List<Patient> patients = new ArrayList<>();

    @Override
    public List<Patient> patientList() {
        System.out.println(patients);
        return patients;
    }

    @Override
    public Patient addPatient(Patient patient) {
        patients.add(new Patient(patient.getIdPatient(), patient.getLastName(), patient.getFirstName()));
        return patient;
    }

    @Override
    public Patient getPatient(int idPatient) {
        System.out.println(patients.get(idPatient));
        return null;
    }

    @Override
    public Patient deletePatient(int idPatient) {
        Patient patient = new Patient();
        patients.remove(patient.getIdPatient());
        return null;
    }

    @Override
    public Patient updatePatient(int idPatient, Patient patient) {
        patients.set(idPatient, patient);
        return null;
    }
}
