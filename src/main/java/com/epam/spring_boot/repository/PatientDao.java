package com.epam.spring_boot.repository;

import com.epam.spring_boot.DTO.Patient;

import java.util.List;

public interface PatientDao {

    List<Patient> patientList();
    Patient addPatient(Patient patient);
    Patient getPatient(int idPatient);
    Patient deletePatient(int idPatient);
    Patient updatePatient(int idPatient, Patient patient);
}
