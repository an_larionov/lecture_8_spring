package com.epam.spring_boot;

import com.epam.spring_boot.DTO.Patient;
import com.epam.spring_boot.service.PatientService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class ExampleApplications {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(ExampleApplications.class, args);

        PatientService patientService = applicationContext.getBean(PatientService.class);
        patientService.addPatient(new Patient(1, "Sidorov", "Ivan"));
        patientService.addPatient(new Patient(2, "Petrov", "Pety"));
        patientService.addPatient(new Patient(3, "Klinov", "Zheka"));
        patientService.patientList();
        patientService.deletePatient(1);
        patientService.patientList();
        patientService.updatePatient(1, new Patient(3, "Klinov", "Zh"));
        patientService.patientList();
        patientService.getPatient(1);
    }
}
