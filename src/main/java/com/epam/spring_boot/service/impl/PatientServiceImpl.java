package com.epam.spring_boot.service.impl;

import com.epam.spring_boot.DTO.Patient;
import com.epam.spring_boot.repository.PatientDao;
import com.epam.spring_boot.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    PatientDao patientDao;

    @Override
    public List<Patient> patientList() {
        return this.patientDao.patientList();
    }

    @Override
    public Patient addPatient(Patient patient) {
        return this.patientDao.addPatient(patient);
    }

    @Override
    public Patient getPatient(int idPatient) {
        return this.patientDao.getPatient(idPatient);
    }

    @Override
    public Patient deletePatient(int idPatient) {
        return this.patientDao.deletePatient(idPatient);
    }

    @Override
    public Patient updatePatient(int idPatient, Patient patient) {
        return this.patientDao.updatePatient(idPatient, patient);
    }
}
